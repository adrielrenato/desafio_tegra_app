import React, { Component } from 'react';
import FilterFlight from './components/pages/flight/FilterFlight'
import FlightListing from './components/pages/flight/flightListing/FlightListing'

class App extends Component {
  constructor() {
    super();
    this.state = {
      flights: []
    }
  }

  setFlights(flights) {
    this.setState({
      flights: flights
    })
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-dark bg-info">
          <span className="navbar-brand mb-0 h1">Tegra - Api Voadora</span>
        </nav>
        <div className="container">
          <FilterFlight setFlights={this.setFlights.bind(this)}></FilterFlight>
          {this.state.flights.map(flight => {
            return <FlightListing flight={flight}></FlightListing>
          })}
        </div>
      </div>
    );
  }
}

export default App;
