import Select from 'react-select';
import React, { Component } from 'react';
import ptBR from 'date-fns/locale/pt-BR';
import moment from 'moment'
import DatePicker, { registerLocale, setDefaultLocale } from 'react-datepicker';
import { getFlights } from '../../actions/FlightListing';
import { getCompanies } from '../../actions/FlightFilter';
import 'react-datepicker/dist/react-datepicker.css';
import '../../../stylesheets/FlightFilter.css';
import sortBy from 'sort-by'

class ListingFlights extends Component {
  constructor(props) {
    super(props);

    registerLocale('ptBR', ptBR);
    setDefaultLocale('ptBR');

    this.state = {
      airport_from: null,
      airport_destiny: null,
      embarkation_date_selected: null,
      embarkation_date_label: null,
      embarkation_date_value: null,
      order_by: { value: 'total_price', label: 'Preço total' },
      cities: [],
      orders: [
        { value: 'total_price', label: 'Preço total' },
        { value: 'total_duration', label: 'Tempo Total de voo' }
      ]
    };

    getCompanies().then(data => { 
      let cities = [];
      for(var index in data) {
        cities.push({ value: data[index].aeroporto, label: data[index].cidade });
      }

      this.setState({ cities: cities });
    });

    this.onChangeAirportFrom = this.onChangeAirportFrom.bind(this);
    this.onChangeAirportDestiny = this.onChangeAirportDestiny.bind(this);
    this.onChangeDatepicker = this.onChangeDatepicker.bind(this);
    this.onChangeOrderBy = this.onChangeOrderBy.bind(this);
  }

  onChangeAirportFrom(event) {
    let target = event;
    this.setState({ airport_from: target });
  }

  onChangeAirportDestiny(event) {
    let target = event;
    this.setState({ airport_destiny: target });
  }

  onChangeDatepicker(event) {
    this.setState({ 
      embarkation_date_label: moment(event).format('DD/MM/YYYY'), 
      embarkation_date_value: moment(event).format('YYYY-MM-DD'),
      embarkation_date_selected: event
    });
  }

  onChangeOrderBy(event) {
    let target = event;
    this.setState({ order_by: target });
  }

  submitHandler = e => {
    e.preventDefault()

    if (this.state['airport_from'] && this.state['airport_destiny'] && this.state['embarkation_date_value']) {
      getFlights(this.state)
        .then(flights => {
          flights.forEach(flight => {
            let totalPrice = 0
            let totalDuration = 0

            flight.voos.forEach(element => {
              const startDate = moment(new Date(element.data_saida + ' ' + element.saida));
              const timeEnd = moment(new Date(element.data_saida + ' ' + element.chegada));
              const diff = timeEnd.diff(startDate);

              totalDuration += diff;
              totalPrice += parseFloat(element.valor)
            });

            flight.total_price = totalPrice
            flight.total_duration = totalDuration
          });

          this.props.setFlights(flights.sort(sortBy(this.state.order_by.value)))
        })
    }
  }

  render() {
    return (
      <div className="mt-3">
        <form onSubmit={this.submitHandler}>
            <div className="form-row justify-content-center">
              <div className="form-group col-md-2 z-index-1500">
                <Select name="airport_from" id="airport_from" placeholder="Origem" isClearable={true} onChange={this.onChangeAirportFrom} options={this.state.cities} value={this.state.airport_from}></Select>
              </div>
              <div className="form-group col-md-2 z-index-1500">
                <Select name="airport_destiny" id="airport_destiny" placeholder="Destino" isClearable={true} onChange={this.onChangeAirportDestiny} options={this.state.cities} value={this.state.airport_destiny}></Select>
              </div>
              <div className="form-group col-md-2 z-index-1500">
                <DatePicker name="embarkation_date" placeholderText="Data de saída" locale='ptBR' selected={this.state.embarkation_date_selected} todayButton={"Hoje"} className="form-control" dateFormat="dd/MM/yyyy" onChange={this.onChangeDatepicker} value={this.state.embarkation_date_label}></DatePicker>
              </div>
              <div className="form-group col-md-2 z-index-1500">
                <button type="submit" className="btn btn-primary" onClick={getFlights}>Buscar</button>
              </div>
              <div className="form-group col-md-4 z-index-1500">
                <Select name="order_by" inputId="order_by" placeholder="Ordenado por" options={this.state.orders} value={this.state.order_by} onChange={this.onChangeOrderBy}></Select>
              </div>
            </div>
          </form>
      </div>
    );
  }
}
export default ListingFlights;
