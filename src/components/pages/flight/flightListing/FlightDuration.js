import React, { Component } from 'react';
import moment from 'moment'

class FlightDuration extends Component {
    render() {
        const startDate = moment(new Date(this.props.flightInfo.data_saida + ' ' + this.props.flightInfo.saida));
        const timeEnd = moment(new Date(this.props.flightInfo.data_saida + ' ' + this.props.flightInfo.chegada));
        const diff = timeEnd.diff(startDate);
        const diffDuration = moment.duration(diff);

        return (
            <small>Duração do voo: 
                <strong className="text-weight"> {((diffDuration.hours()).toString()).padStart(2, '0') + ':' + ((diffDuration.minutes()).toString()).padStart(2, '0')}</strong>
            </small>
        );
    }
}

export default FlightDuration;