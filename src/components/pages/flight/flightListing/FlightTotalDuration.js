import React, { Component } from 'react';
import moment from 'moment'

class FlightTotalDuration extends Component {
    render() {
        const diffDuration = moment.duration(this.props.totalDuration);
        return (
            <div>
                <span className="text-info h6">Tempo total de voo: 
                    <span className="h5 text-dark"> {((diffDuration.hours()).toString()).padStart(2, '0') + ':' + ((diffDuration.minutes()).toString()).padStart(2, '0')}</span>
                </span>
            </div>
        );
    }
}

export default FlightTotalDuration;