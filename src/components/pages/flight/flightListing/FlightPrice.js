import React, { Component } from 'react';
import FlightTotalDuration from './FlightTotalDuration';

class FlightPrice extends Component {    
    render() {
        return (
            <div className="col-md-4 align-self-center text-center">
                <FlightTotalDuration totalDuration={this.props.totalDuration}></FlightTotalDuration>
                <p>Preço por pessoa, ida</p>
                <small className="text-info h5">R$</small> <span className="h2">{this.props.totalPrice.toFixed(2)}
                </span>
            </div>
        );
    }
}

export default FlightPrice;