import React, { Component } from 'react';

class FlightType extends Component {
    render() {
        return (
            <small className="h6 float-left">
                {this.props.flights.length === 1 ? 'Voo Direto' : this.props.flights.length + ' Escalas'}
            </small>
        );
    }
}

export default FlightType;