import React, { Component } from 'react';

class FlightDate extends Component {
    render() {
        let date;
        date = this.props.date.split('-')

        return (
            <small className="text-muted mr-4">
                { date[2] + '/' + date[1] + '/' + date[0] }
            </small>
        );
    }
}

export default FlightDate;