import React, { Component } from 'react';

class FlightTime extends Component {
    render() {
        return (
            <div className="info-time float-left">
                <span className="h6">{this.props.flightTime.saida}</span>
                <span> -> </span>
                <span className="h6">{this.props.flightTime.chegada}</span>
            </div>
        );
    }
}

export default FlightTime;