import React, { Component } from 'react';

class FlightInfo extends Component {
    render() {
        return (
            <div className="ml-4 float-left w-50">
                <span className="h5 float-left">{this.props.flightInfo.origem}</span>
                <div className="simulate-range float-left"></div>
                <span className="h5 mr-4 float-left">{this.props.flightInfo.destino}</span>
            </div>
        );
    }
}

export default FlightInfo;