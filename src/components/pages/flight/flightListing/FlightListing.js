import React, { Component } from 'react';
import FlightTime from './FlightTime';
import FlightPrice from './FlightPrice';
import FlightInfo from './FlightInfo';
import FlightDuration from './FlightDuration';
import FlightDate from './FlightDate';
import Airport from './Airport';
import '../../../../stylesheets/FlightListing.css';

class FlightListing extends Component {
  
  render() {
    return (
      <div className="row mt-3">
        <div className="col-md-12">
          <div className="card bg-light border-info">
            <div className="card-body">
              <div className="row no-gutters">
                <div className="col-md-8">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="info-date float-left">
                        <span className="h4 mr-2">Ida</span>
                        <FlightDate date={this.props.flight.date}></FlightDate>
                      </div>
                      <Airport origin={this.props.flight.origem} destiny={this.props.flight.destino} flights={this.props.flight.voos}></Airport>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <ul className="list-group w-90">
                      {this.props.flight.voos.map(flightInfo => {
                        const date = flightInfo.data_saida.split('-');

                        return <li className="list-group-item active text-white">
                          <FlightTime flightTime={flightInfo}></FlightTime>
                          <FlightDuration flightInfo={flightInfo}></FlightDuration>
                          <li className="list-group-item text-dark">
                            <FlightInfo flightInfo={flightInfo}></FlightInfo>
                            <small>Data saída: <b>{ date[2] + '/' + date[1] + '/' + date[0] }</b></small>
                          </li>
                        </li>
                      })}
                      </ul>
                    </div>
                  </div>
                </div>
                <FlightPrice totalPrice={this.props.flight.total_price} totalDuration={this.props.flight.total_duration}></FlightPrice>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FlightListing;
