import React, { Component } from 'react';
import FlightType from './FlightType';

class Airport extends Component {
    render() {
        return (
            <div className="info-company mt-1">
                <span className="h5 float-left">{this.props.origin}</span>
                <div className="simulate-range float-left"></div>
                <span className="h5 mr-4 float-left">{this.props.destiny}</span>
                <FlightType flights={this.props.flights}></FlightType>
            </div>
        );
    }
}

export default Airport;