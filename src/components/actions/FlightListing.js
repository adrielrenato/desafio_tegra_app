import axios from 'axios'

export const getFlights = (state) => {
    const url = 'https://api-voadora.dev.tegra.com.br/flight';

    if (state['airport_from'] && state['airport_destiny'] && state['embarkation_date_value']) {
        const params = {
            "from": state['airport_from'].value,
            "to": state['airport_destiny'].value,
            "date": state['embarkation_date_value']
        }

        return axios.post(url, params)
            .then(response => response.data);
    }
}