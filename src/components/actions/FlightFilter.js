export const getCompanies = () => {
    const url = 'https://api-voadora.dev.tegra.com.br/flight/companies';

    return fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json());
}