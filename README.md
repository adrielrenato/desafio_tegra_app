# desafio_tegra_app

Requisitos não funcionais
- Git (Terminal)
- NodeJs
- Conexão com internet

Para rodar a aplicação siga os passos abaixo:

1 - Abra o terminal de comando na pasta do projeto. Em seguida coloque o seguinte comando e clique em enter para começar.
npm install

2 - Para visualizar o aplicativo, será necessário colocar o seguinte comando no terminal e clique em enter para começar. Por padrão a pagina será http://localhost:3000, porém fique de olho no terminal na parte "local", que ele apresentará a página correta.
npm start

3 - Pronto, o sistema está pronto para testes.